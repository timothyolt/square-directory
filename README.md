2) Build Tools & Versions Used: 
    * IntelliJ Ultimate 2019.3.4 
    * Kotlin 1.3.71 IntelliJ plugin + stdlib + runtime
    * Gradle 5.4.1 
    * Tested on a Pixel 4XL running Android 10, March 5th patch
3) Your Focus Areas. <br>
    My primary focus areas were the architecture and data flow. I used patterns vaguely inspired by MVI and 
    Clean Architecture, using Kotlin Coroutines and Flows where some people use RxJava. Usually, I'm using Kotlin MPP to build a multiplatform version of the 'lib' module you will see. I bumped into some minor tooling differences switching back to basic Kotlin/JVM. <br> 
    I did not focus too heavily on the UI.
4) Copied-in code or copied-in dependencies. <br>
    I copied a couple one-liners for previous projects of mine, all in the View/Activity layers.

5) Tablet / phone focus. <br>
    Focused on the Pixel 4XL phone, specifically. I used a few theme attributes and ConstraintLayouts, which should make it compatible with tablets. (nothing too crazy though.)
6) How long you spent on the project. <br>
    I spent some time getting my tools set up and putting basic smoke tests together. Longer than I expected there, due to the switch from MPP. After I got going though, I spent 4 hours on the basics and 2 hours on the details.
7) Anything else you want us to know. <br> 
    If I had just a little more time, the next thing I would add is a reactive search!