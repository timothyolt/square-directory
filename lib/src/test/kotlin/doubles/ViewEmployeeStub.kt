package materialize.square.directory.test.doubles

import materialize.square.directory.*

class ViewEmployeeStub(var employee: EmployeeDetail) : ViewEmployee {

    var error: String? = null

    override suspend fun invoke(id: EmployeeId): EmployeeDetail {
        val error = error
        return if (error != null) error(error)
        else employee
    }
}