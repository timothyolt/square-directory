package materialize.square.directory.test.doubles

import materialize.square.directory.EmployeeList
import materialize.square.directory.EmployeeSummary
import materialize.square.directory.ViewEmployeeList

class ViewEmployeeListStub(employees: List<EmployeeSummary>) : ViewEmployeeList {

    val employees: MutableList<EmployeeSummary> = employees.toMutableList()

    var error: String? = null

    override suspend fun invoke(): EmployeeList {
        val error = error
        return if (error != null) error(error)
        else EmployeeList(employees)
    }
}