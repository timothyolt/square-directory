package materialize.square.directory.test.doubles

import materialize.square.directory.Employee
import materialize.square.directory.EmployeeId
import materialize.square.directory.EmployeeRepository

class EmployeeRepositoryFake(val list: List<Employee>) : EmployeeRepository {

    override suspend fun all(): List<Employee> = list

    override suspend fun get(id: EmployeeId): Employee? {
        return list.find { it.id == id }
    }

}