package materialize.square.directory.test

import kotlinx.coroutines.runBlocking
import materialize.square.directory.*
import materialize.square.directory.mobile.*
import materialize.square.directory.test.doubles.ViewEmployeeStub
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class EmployeePresenterTest {

    @Test fun `Given new, When loader starts, Then emits Loading`() {
        val id = EmployeeId("")
        val viewEmployee = ViewEmployeeStub(
            EmployeeDetail(id, "Not empty", "team", null, null, "nobody@nowhere.net", null, EmployeeType.Contractor)
        )

        val states = mutableListOf<EmployeeModel>()
        val presenter = EmployeePresenter(viewEmployee, { phone -> phone }) { model ->
            states.add(model)
        }

        runBlocking { presenter.invoke(EmployeeIntent.Load(id)) }

        assertEquals(EmployeeModel.Loading, states.first())
    }

    @Test fun `Given new, When loader completes, Then emits Done with completion`() {
        val id = EmployeeId("Tom")
        val name = "Tom"
        val viewEmployee = ViewEmployeeStub(
            EmployeeDetail(id, name, "A-Team", null, null, "tom@materialize.tech", null, EmployeeType.PartTime)
        )

        val states = mutableListOf<EmployeeModel>()
        val presenter = EmployeePresenter(viewEmployee, { phone -> phone}) { model ->
            states.add(model)
        }

        runBlocking { presenter.invoke(EmployeeIntent.Load(id)) }
        val last = states.last()

        assertTrue(last is EmployeeModel.Done)
        assertEquals(id, last.id)
    }

    @Test fun `Given new, When loader throws, Then emits Error with empty list`() {
        val message = "Throwing"
        val viewEmployee = object : ViewEmployee {
            override suspend fun invoke(id: EmployeeId) = error(message)
        }

        val states = mutableListOf<EmployeeModel>()
        val presenter = EmployeePresenter(viewEmployee, { phone -> phone }) { model ->
            states.add(model)
        }

        runBlocking { presenter.invoke(EmployeeIntent.Load(EmployeeId("anything"))) }

        assertEquals(EmployeeModel.Error(message), states.last())
    }
}