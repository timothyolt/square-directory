package materialize.square.directory.test

import kotlinx.coroutines.runBlocking
import materialize.square.directory.*
import materialize.square.directory.test.doubles.EmployeeRepositoryFake
import kotlin.test.*

class ViewEmployeeListInteractorTest {

    val repository = EmployeeRepositoryFake(
        listOf(
            Employee(
                EmployeeId("Tim"),
                "Tim",
                "A-Team",
                EmployeeDescription(
                    PhotoId(
                        "https://en.gravatar.com/userimage/38436022/35454899b4f99140c7247cc108791074.png?size=512",
                        "https://en.gravatar.com/userimage/38436022/35454899b4f99140c7247cc108791074.png?size=128"
                    ),
                    "6782233039",
                    "tim@materialize.tech",
                    "Makes neat apps.",
                    EmployeeType.FullTime
                )
            ),
            Employee(
                EmployeeId("Tom"),
                "Tom",
                "A-Team",
                EmployeeDescription(null, null, "tom@materialize.tech", null, EmployeeType.PartTime)
            )
        )
    )

    @Test fun `Given EmployeeList, When viewing, Then the result contains all Employee names`() {
        val employeeList = runBlocking { ViewEmployeeListInteractor(repository).invoke() }
        val repositoryNames = repository.list.map { EmployeeSummary(it.id, it.name, it.team, it.description.photoId) }
        assertTrue(employeeList.list.containsAll(repositoryNames))
    }
}