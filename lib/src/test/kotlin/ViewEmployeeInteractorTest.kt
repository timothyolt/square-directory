package materialize.square.directory.test

import kotlinx.coroutines.runBlocking
import materialize.square.directory.*
import materialize.square.directory.test.doubles.EmployeeRepositoryFake
import kotlin.test.*

class ViewEmployeeInteractorTest {

    val repository = EmployeeRepositoryFake(
        listOf(
            Employee(
                EmployeeId("Tim"),
                "Tim",
                "A-Team",
                EmployeeDescription(
                    PhotoId(
                        "https://en.gravatar.com/userimage/38436022/35454899b4f99140c7247cc108791074.png?size=512",
                        "https://en.gravatar.com/userimage/38436022/35454899b4f99140c7247cc108791074.png?size=128"
                    ),
                    "6782233039",
                    "tim@materialize.tech",
                    "Makes neat apps.",
                    EmployeeType.FullTime
                )
            ),
            Employee(
                EmployeeId("Tom"),
                "Tom",
                "A-Team",
                EmployeeDescription(null, null, "tom@materialize.tech", null, EmployeeType.PartTime)
            )
        )
    )

    @Test
    fun `Given EmployeeRepository, When viewing, Then the result contains the EmployeeDetails`() {
        val employeeDetail = runBlocking { ViewEmployeeInteractor(repository).invoke(EmployeeId("Tom")) }
        assertEquals(employeeDetail.id, EmployeeId("Tom"))
    }
}