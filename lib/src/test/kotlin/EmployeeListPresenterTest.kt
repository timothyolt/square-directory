package materialize.square.directory.test

import kotlinx.coroutines.runBlocking
import materialize.square.directory.EmployeeId
import materialize.square.directory.EmployeeSummary
import materialize.square.directory.ViewEmployeeList
import materialize.square.directory.mobile.EmployeeListIntent
import materialize.square.directory.mobile.EmployeeListModel
import materialize.square.directory.mobile.EmployeeListPresenter
import materialize.square.directory.test.doubles.ViewEmployeeListStub
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class EmployeeListPresenterTest {

    @Test fun `Given new, When loader starts, Then emits Loading with empty list`() {
        val viewEmployees = ViewEmployeeListStub(listOf(
            EmployeeSummary(EmployeeId(""), "Not empty", "team", null)
        ))

        val states = mutableListOf<EmployeeListModel>()
        val presenter = EmployeeListPresenter(viewEmployees) { model ->
            states.add(model)
        }

        runBlocking { presenter.invoke(EmployeeListIntent.Load) }

        assertEquals(EmployeeListModel.Loading(emptyList()), states.first())
    }

    @Test fun `Given new, When loader completes, Then emits Done with completion`() {
        val employees = listOf(
            EmployeeSummary(EmployeeId(""), "Tom", "A-Team", null)
        )
        val viewEmployees = ViewEmployeeListStub(employees)

        val states = mutableListOf<EmployeeListModel>()
        val presenter = EmployeeListPresenter(viewEmployees) { model ->
            states.add(model)
        }

        runBlocking { presenter.invoke(EmployeeListIntent.Load) }
        val last = states.last()

        assertTrue(last is EmployeeListModel.Done)
        assertEquals(employees, last.list)
    }

    @Test fun `Given new, When loader throws, Then emits Error with empty list`() {
        val message = "Throwing"
        val viewEmployees = object : ViewEmployeeList {
            override suspend fun invoke() = error(message)
        }

        val states = mutableListOf<EmployeeListModel>()
        val presenter = EmployeeListPresenter(viewEmployees) { model ->
            states.add(model)
        }

        runBlocking { presenter.invoke(EmployeeListIntent.Load) }
        val last = states.last()

        assertEquals(EmployeeListModel.Error(emptyList(), message), last)
    }

    @Test fun `Given existing list, When loader starts, Then emits Loading with empty list`() {
        val existing = listOf(
            EmployeeSummary(EmployeeId(""), "Not empty", "team", null)
        )
        val viewEmployees = ViewEmployeeListStub(existing)

        val states = mutableListOf<EmployeeListModel>()
        val presenter = EmployeeListPresenter(viewEmployees) { model ->
            states.add(model)
        }
        runBlocking { presenter.invoke(EmployeeListIntent.Load) }
        states.clear()

        runBlocking { presenter.invoke(EmployeeListIntent.Load) }

        assertEquals(EmployeeListModel.Loading(existing), states.first())
    }

    @Test fun `Given existing list, When loader completes, Then emits Done with completion`() {
        val existing = listOf(
            EmployeeSummary(EmployeeId(""), "Not empty", "team", null)
        )
        val viewEmployees = ViewEmployeeListStub(existing)

        val states = mutableListOf<EmployeeListModel>()
        val presenter = EmployeeListPresenter(viewEmployees) { model ->
            states.add(model)
        }
        runBlocking { presenter.invoke(EmployeeListIntent.Load) }
        states.clear()

        val newEmployees = listOf(EmployeeSummary(EmployeeId("DE"), "Different employee", "other team", null))
        viewEmployees.employees.clear()
        viewEmployees.employees.addAll(newEmployees)

        runBlocking { presenter.invoke(EmployeeListIntent.Load) }
        val last = states.last()

        assertTrue(last is EmployeeListModel.Done)
        assertEquals(newEmployees, last.list)
    }

    @Test fun `Given existing list, When loader throws, Then emits Error with empty list`() {
        val existing = listOf(
            EmployeeSummary(EmployeeId(""), "Not empty", "team", null)
        )
        val viewEmployees = ViewEmployeeListStub(existing)

        val states = mutableListOf<EmployeeListModel>()
        val presenter = EmployeeListPresenter(viewEmployees) { model ->
            states.add(model)
        }
        runBlocking { presenter.invoke(EmployeeListIntent.Load) }
        states.clear()
        val message = "Simulated error"
        viewEmployees.error = message

        runBlocking { presenter.invoke(EmployeeListIntent.Load) }
        val last = states.last()

        assertEquals(EmployeeListModel.Error(existing, message), last)
    }
}