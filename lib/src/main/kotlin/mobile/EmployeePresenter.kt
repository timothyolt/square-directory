package materialize.square.directory.mobile

import kotlinx.coroutines.flow.flow
import materialize.square.directory.*
import materialize.square.directory.util.invoke

sealed class EmployeeIntent {
    data class Load(val id: EmployeeId) : EmployeeIntent()
}

sealed class EmployeeModel {
    object Loading : EmployeeModel()
    data class Done(
        val id: EmployeeId,
        val name: String,
        val team: String,
        val photoId: String?,
        val phoneNumber: String?,
        val emailAddress: String,
        val biography: String?,
        val type: String
    ) : EmployeeModel()
    data class Error(val error: String) : EmployeeModel()
}

class EmployeePresenter(
    private val viewEmployee: ViewEmployee,
    private val phoneFormatter: (String) -> String,
    private val render: EmployeeRenderer
) {
    private val state = EmployeeState()

    suspend operator fun invoke(intent: EmployeeIntent) = when (intent) {
        is EmployeeIntent.Load -> render(load(intent.id))
    }

    private fun load(id: EmployeeId) = flow {
        emit(state.update(EmployeeModel.Loading))
        emit(state.update(loadEmployee(id)))
    }

    private suspend fun loadEmployee(id: EmployeeId) = try {
        val employee = viewEmployee(id)
        employee.toDetailModel()
    } catch (e: Exception) {
        EmployeeModel.Error(e.message ?: "Error")
    }

    private fun EmployeeDetail.toDetailModel() = EmployeeModel.Done(
        id = id,
        name = name,
        team = team,
        photoId = photoId?.toDetailUri(),
        phoneNumber = phoneNumber?.toFormattedPhoneNumber(phoneFormatter),
        emailAddress = emailAddress,
        biography = biography,
        type = type.toHumanString()
    )

    private fun PhotoId.toDetailUri() = uri

    private fun String.toFormattedPhoneNumber(formatter: (String) -> String) = formatter(this)

    private fun EmployeeType.toHumanString() = when(this) {
        EmployeeType.FullTime -> "Full Time"
        EmployeeType.PartTime -> "Part Time"
        EmployeeType.Contractor -> "Contractor"
    }
}

private typealias EmployeeRenderer = (EmployeeModel) -> Unit

private class EmployeeState {

    private var state: EmployeeModel = EmployeeModel.Loading

    fun update(changes: EmployeeModel): EmployeeModel {
        state = changes
        return state
    }

}