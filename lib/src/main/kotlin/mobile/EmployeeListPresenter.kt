package materialize.square.directory.mobile

import materialize.square.directory.util.invoke
import kotlinx.coroutines.flow.flow
import materialize.square.directory.EmployeeSummary
import materialize.square.directory.ViewEmployeeList

sealed class EmployeeListIntent {
    object Load : EmployeeListIntent()
    object ErrorDismissed : EmployeeListIntent()
}

sealed class EmployeeListModel {
    abstract val list: List<EmployeeSummary>

    data class Loading(override val list: List<EmployeeSummary>) : EmployeeListModel()
    data class Done(override val list: List<EmployeeSummary>) : EmployeeListModel()
    data class Error(override val list: List<EmployeeSummary>, val error: String) : EmployeeListModel()
}

class EmployeeListPresenter(
    private val viewEmployeeList: ViewEmployeeList,
    private val render: EmployeeListRenderer
) {

    private val state = EmployeeListState()

    suspend operator fun invoke(intent: EmployeeListIntent) {
        when (intent) {
            EmployeeListIntent.Load -> render(load())
            EmployeeListIntent.ErrorDismissed -> render(dismissError())
        }
    }

    private fun load() = flow {
        emit(state.update(EmployeeListPartial.Loading))
        emit(state.update(loadEmployeeList()))
    }

    private suspend fun loadEmployeeList() = try {
        val employees = viewEmployeeList()
        if (employees.list.isEmpty()) EmployeeListPartial.Error(Throwable("No Employees Found"))
        else EmployeeListPartial.Done(employees.list)
    } catch (e: Exception) {
        EmployeeListPartial.Error(e)
    }

    private fun dismissError() = state.update(EmployeeListPartial.ErrorDismiss)
}

private class EmployeeListState {

    private var state: EmployeeListModel = EmployeeListModel.Done(emptyList())

    fun update(changes: EmployeeListPartial): EmployeeListModel {
        state = reduce(state, changes)
        return state
    }

    private fun reduce(previous: EmployeeListModel, changes: EmployeeListPartial) = when (changes) {
        is EmployeeListPartial.Loading -> EmployeeListModel.Loading(previous.list)
        is EmployeeListPartial.Done -> EmployeeListModel.Done(changes.list)
        is EmployeeListPartial.Error -> EmployeeListModel.Error(previous.list, changes.throwable.message ?: "Error")
        EmployeeListPartial.ErrorDismiss -> EmployeeListModel.Done(previous.list)
    }
}

private typealias EmployeeListRenderer = (EmployeeListModel) -> Unit

private sealed class EmployeeListPartial {
    object Loading : EmployeeListPartial()
    object ErrorDismiss : EmployeeListPartial()
    data class Done(val list: List<EmployeeSummary>) : EmployeeListPartial()
    data class Error(val throwable: Throwable) : EmployeeListPartial()
}
