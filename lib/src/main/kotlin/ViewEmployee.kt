package materialize.square.directory

data class EmployeeDetail(
    val id: EmployeeId,
    val name: String,
    val team: String,
    val photoId: PhotoId?,
    val phoneNumber: String?,
    val emailAddress: String,
    val biography: String?,
    val type: EmployeeType
)

interface ViewEmployee {
    suspend operator fun invoke(id: EmployeeId): EmployeeDetail
}

class ViewEmployeeInteractor(private val employees: EmployeeRepository) : ViewEmployee {

    override suspend fun invoke(id: EmployeeId): EmployeeDetail {
        val employee = employees.get(id) ?: error("Employee Not Found")
        return employee.toDetail()
    }

    private fun Employee.toDetail() = EmployeeDetail(
        id = id,
        name = name,
        team = team,
        photoId = description.photoId,
        phoneNumber = description.phoneNumber,
        emailAddress = description.emailAddress,
        biography = description.biography,
        type = description.type
    )

}
