package materialize.square.directory.list

import kotlinx.coroutines.delay
import materialize.square.directory.Employee
import materialize.square.directory.EmployeeId
import materialize.square.directory.EmployeeRepository
import kotlin.random.Random

class EmployeeListRepositorySimulator(val list: List<Employee>) : EmployeeRepository {

    override suspend fun all(): List<Employee> {
        delay(500)
        if (Random.nextBoolean())
            throw error("Simulated error")
        return list
    }

    override suspend fun get(id: EmployeeId): Employee? {
        delay(500)
        if (Random.nextBoolean())
            throw error("Simulated error")
        return list.find { it.id == id }
    }

}