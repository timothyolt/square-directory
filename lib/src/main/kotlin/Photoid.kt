package materialize.square.directory

data class PhotoId(val uri: String, val thumbnailUri: String)