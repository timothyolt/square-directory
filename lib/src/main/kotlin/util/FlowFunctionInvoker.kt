package materialize.square.directory.util

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.collect

suspend operator fun <T> ((T) -> Unit).invoke(flow: Flow<T>) = flow.collect { model -> this(model) }