package materialize.square.directory

data class EmployeeList(val list: List<EmployeeSummary>)

data class EmployeeSummary(val id: EmployeeId, val name: String, val team: String, val photoId: PhotoId?)

interface ViewEmployeeList {
    suspend operator fun invoke(): EmployeeList
}

class ViewEmployeeListInteractor(private val employees: EmployeeRepository): ViewEmployeeList {
    override suspend fun invoke(): EmployeeList {
        return EmployeeList(employees.all().map { it.toSummary() })
    }

    private fun Employee.toSummary() = EmployeeSummary(id, name, team, description.photoId)
}
