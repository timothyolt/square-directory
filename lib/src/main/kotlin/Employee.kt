package materialize.square.directory

class Employee(val id: EmployeeId, val name: String, val team: String, val description: EmployeeDescription)

data class EmployeeDescription(
    val photoId: PhotoId?,
    val phoneNumber: String?,
    val emailAddress: String,
    val biography: String?,
    val type: EmployeeType
)

enum class EmployeeType {
    FullTime, PartTime, Contractor
}

interface EmployeeRepository {
    suspend fun all(): List<Employee>
    suspend fun get(id: EmployeeId): Employee?
}
