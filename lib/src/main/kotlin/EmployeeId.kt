package materialize.square.directory

data class EmployeeId(val raw: String)