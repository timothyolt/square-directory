package materialize.square.directory.app.repository

import com.squareup.moshi.JsonClass
import materialize.square.directory.*
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.http.GET

interface EmployeeRetrofitService {
    @GET("employees.json") suspend fun employees(): JsonEmployeesList
}

@JsonClass(generateAdapter = true)
data class JsonEmployeesList(
    val employees: List<JsonEmployee>
)

@JsonClass(generateAdapter = true)
data class JsonEmployee(
    val uuid: String,
    val full_name: String,
    val phone_number: String?,
    val email_address: String,
    val biography: String?,
    val photo_url_small: String?,
    val photo_url_large: String?,
    val team: String,
    val employee_type: JsonEmployeeType
)

enum class JsonEmployeeType {
    FULL_TIME, PART_TIME, CONTRACTOR
}

class MemoryCachedRetrofitEmployeeRepository : EmployeeRepository {

    private val retrofit = Retrofit.Builder()
        .addConverterFactory(MoshiConverterFactory.create())
        .baseUrl("https://s3.amazonaws.com/sq-mobile-interview/")
        .build()

    private val employeeRetrofitService = retrofit.create(EmployeeRetrofitService::class.java)

    private var employees: Map<EmployeeId, Employee>? = null

    override suspend fun all() = getOrFetchAll().values.toList()

    private suspend fun getOrFetchAll() = employees ?: fetchAll()

    private suspend fun fetchAll(): Map<EmployeeId, Employee> {
        val employees = employeeRetrofitService
            .employees()
            .employees
            .map { it.toEmployee() }
            .associateBy { it.id }
        this.employees = employees
        return employees
    }

    private fun JsonEmployee.toEmployee() = Employee(
        id = EmployeeId(uuid),
        name = full_name,
        team = team,
        description = EmployeeDescription(
            photoId = getPhotoId(),
            biography = biography,
            emailAddress = email_address,
            phoneNumber = phone_number,
            type = employee_type.toEmployeeType()
        )
    )

    private fun JsonEmployee.getPhotoId(): PhotoId? {
        if (photo_url_large == null && photo_url_small == null) return null
        return PhotoId(
            uri = photo_url_large ?: photo_url_small!!,
            thumbnailUri = photo_url_small ?: photo_url_large!!
        )
    }

    private fun JsonEmployeeType.toEmployeeType() = when(this) {
        JsonEmployeeType.FULL_TIME -> EmployeeType.FullTime
        JsonEmployeeType.PART_TIME -> EmployeeType.PartTime
        JsonEmployeeType.CONTRACTOR -> EmployeeType.Contractor
    }

    override suspend fun get(id: EmployeeId) = getOrFetchAll()[id]

}
