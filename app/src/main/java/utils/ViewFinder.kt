package materialize.square.directory.app.utils

import android.view.View
import androidx.annotation.IdRes

interface ViewFinder {
    fun <T : View> findViewById(@IdRes id: Int): T
}