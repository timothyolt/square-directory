package materialize.square.directory.app

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.lifecycle.lifecycleScope
import com.squareup.picasso.Picasso
import kotlinx.coroutines.launch
import materialize.square.directory.EmployeeId
import materialize.square.directory.app.factory.EmployeeControllerFactory
import materialize.square.directory.app.utils.ViewFinder
import materialize.square.directory.mobile.EmployeeIntent
import materialize.square.directory.mobile.EmployeeListModel
import materialize.square.directory.mobile.EmployeeModel

class EmployeeActivity : AppCompatActivity(), ViewFinder {

    private val controller = EmployeeControllerFactory().create()

    private var employeeId: EmployeeId? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        employeeId = intent.getEmployeeId() ?: employeeIdError()
        setContentView(R.layout.activity_employee_detail)

        val view = EmployeeView(this, ::finish, ::launchLoad)
        setSupportActionBar(view.toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        controller.setRenderer(view::render)
    }

    override fun onResume() {
        super.onResume()
        launchLoad()
    }

    private fun employeeIdError(): Nothing {
        error("EmployeeActivity requires an EmployeeId")
    }

    private fun launchLoad() {
        val id = employeeId ?: employeeIdError()
        launchLoad(id)
    }

    private fun launchLoad(id: EmployeeId) {
        lifecycleScope.launch {
            controller(EmployeeIntent.Load(id))
        }
    }

    companion object {
        fun createIntent(context: Context, id: EmployeeId): Intent {
            return Intent(context, EmployeeActivity::class.java).putExtra(EXTRA_EMPLOYEE, id.raw)
        }

        private fun Intent.getEmployeeId(): EmployeeId? {
            val raw = getStringExtra(EXTRA_EMPLOYEE) ?: return null
            return EmployeeId(raw)
        }

        private const val EXTRA_EMPLOYEE = "EmployeeId"
    }
}

class EmployeeView(
    private val viewFinder: ViewFinder,
    private val onContinue: () -> Unit,
    private val onTryAgain: () -> Unit
) {

    val toolbar = viewFinder.findViewById<Toolbar>(R.id.toolbar)

    private val image = viewFinder.findViewById<ImageView>(R.id.image)

    private val textName = viewFinder.findViewById<TextView>(R.id.textName)
    private val textTeam = viewFinder.findViewById<TextView>(R.id.textTeam)
    private val textType = viewFinder.findViewById<TextView>(R.id.textType)
    private val labelPhone = viewFinder.findViewById<TextView>(R.id.labelPhone)
    private val textPhone = viewFinder.findViewById<TextView>(R.id.textPhone)
    private val labelEmail = viewFinder.findViewById<TextView>(R.id.labelEmail)
    private val textEmail = viewFinder.findViewById<TextView>(R.id.textEmail)
    private val textBio = viewFinder.findViewById<TextView>(R.id.textBio)

    private val progress = viewFinder.findViewById<ProgressBar>(R.id.progress)

    private val errorText = viewFinder.findViewById<TextView>(R.id.errorMessage)

    private val tryAgainButton = viewFinder.findViewById<Button>(R.id.buttonTryAgain).also {
        it.setOnClickListener { onTryAgain() }
    }

    private val continueButton = viewFinder.findViewById<Button>(R.id.buttonContinue).also {
        it.setOnClickListener { onContinue() }
    }

    fun render(model: EmployeeModel) = when (model) {
        is EmployeeModel.Loading -> {
            hideEmployee()
            hideError()
            showProgress()
        }
        is EmployeeModel.Done -> {
            hideProgress()
            hideError()
            showEmployee(model)
        }
        is EmployeeModel.Error -> {
            hideEmployee()
            hideProgress()
            showError(model.error)
        }
    }

    private fun showEmployee(model: EmployeeModel.Done) {
        textName.visibility = View.VISIBLE
        textName.text = model.name
        textTeam.visibility = View.VISIBLE
        textTeam.text = model.team
        textType.visibility = View.VISIBLE
        textType.text = model.type
        val phone = model.phoneNumber
        if (phone == null) hidePhone()
        else showPhone(phone)
        showEmail(model.emailAddress)
        val bio = model.biography
        if (bio == null) hideBio()
        else showBio(bio)
        val imageId = model.photoId
        if (imageId == null) showImage(R.drawable.ic_profile_default_24dp)
        else showImage(imageId)
    }

    private fun showImage(uri: String) {
        image.visibility = View.VISIBLE
        Picasso.get().load(Uri.parse(uri)).fit().centerCrop().placeholder(R.drawable.ic_profile_default_24dp).into(image)
    }

    private fun showImage(resource: Int) {
        image.visibility = View.VISIBLE
        image.setImageResource(resource)
    }

    private fun hideImage() {
        image.setImageBitmap(null)
        image.visibility = View.GONE
    }

    private fun showPhone(phone: String) {
        labelPhone.visibility = View.VISIBLE
        textPhone.visibility = View.VISIBLE
        textPhone.text = phone
    }

    private fun hidePhone() {
        labelPhone.visibility = View.GONE
        textPhone.visibility = View.GONE
    }

    private fun showEmail(email: String) {
        labelEmail.visibility = View.VISIBLE
        textEmail.visibility = View.VISIBLE
        textEmail.text = email
    }

    private fun hideEmail() {
        labelEmail.visibility = View.GONE
        textEmail.visibility = View.GONE
    }

    private fun showBio(bio: String) {
        textBio.visibility = View.VISIBLE
        textBio.text = bio
    }

    private fun hideBio() {
        textBio.visibility = View.GONE
    }

    private fun hideProgress() {
        progress.visibility = View.GONE
    }

    private fun showProgress() {
        progress.visibility = View.VISIBLE
    }

    private fun hideEmployee() {
        textName.visibility = View.GONE
        textTeam.visibility = View.GONE
        textType.visibility = View.GONE
        hidePhone()
        hideEmail()
        hideBio()
        hideImage()
    }

    private fun hideError() {
        errorText.visibility = View.GONE
        tryAgainButton.visibility = View.GONE
        continueButton.visibility = View.GONE
    }

    private fun showError(message: String) {
        errorText.visibility = View.VISIBLE
        tryAgainButton.visibility = View.VISIBLE
        continueButton.visibility = View.VISIBLE
        errorText.text = message
    }

}
