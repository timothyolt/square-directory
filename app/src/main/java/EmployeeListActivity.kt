package materialize.square.directory.app

import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.google.android.material.snackbar.Snackbar
import com.squareup.picasso.Picasso
import kotlinx.coroutines.launch
import materialize.square.directory.EmployeeId
import materialize.square.directory.EmployeeSummary
import materialize.square.directory.app.factory.EmployeeListControllerFactory
import materialize.square.directory.app.utils.ViewFinder
import materialize.square.directory.mobile.EmployeeListIntent
import materialize.square.directory.mobile.EmployeeListModel
import kotlin.properties.Delegates

class EmployeeListActivity : AppCompatActivity(), ViewFinder {

    private val controller = EmployeeListControllerFactory().create()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_employee_list)

        val view = EmployeeListView(this, ::launchErrorDismissed, ::launchLoad, ::launchEmployeeDetail, layoutInflater)
        setSupportActionBar(view.toolbar)
        controller.setRenderer(view::render)
    }

    override fun onResume() {
        super.onResume()
        launchLoad()
    }

    private fun launchLoad() {
        lifecycleScope.launch {
            controller(EmployeeListIntent.Load)
        }
    }

    private fun launchErrorDismissed() {
        lifecycleScope.launch {
            controller(EmployeeListIntent.ErrorDismissed)
        }
    }

    private fun launchEmployeeDetail(id: EmployeeId) {
        startActivity(EmployeeActivity.createIntent(this, id))
    }

}

class EmployeeListView(
    private val viewFinder: ViewFinder,
    private val onErrorDismiss: () -> Unit,
    private val onReload: () -> Unit,
    onItemClick: (id: EmployeeId) -> Unit,
    layoutInflater: LayoutInflater
) {

    val toolbar = viewFinder.findViewById<Toolbar>(R.id.toolbar)

    private val adapter = EmployeeListAdapter(layoutInflater, onItemClick)

    private val recycler = viewFinder.findViewById<RecyclerView>(R.id.list).also {
        it.adapter = adapter
    }

    private val swipeRefresh = viewFinder.findViewById<SwipeRefreshLayout>(R.id.swipeRefresh).also {
        it.setOnRefreshListener { onReload() }
    }

    private val errorText = viewFinder.findViewById<TextView>(R.id.errorMessage)

    private val progress = viewFinder.findViewById<ProgressBar>(R.id.progress)

    private val tryAgainButton = viewFinder.findViewById<Button>(R.id.buttonTryAgain).also {
        it.setOnClickListener { onReload() }
    }

    fun render(model: EmployeeListModel) {
        adapter.employees = model.list
        swipeRefresh.isRefreshing = when (model) {
            is EmployeeListModel.Loading -> model.list.any()
            is EmployeeListModel.Done, is EmployeeListModel.Error -> false
        }
        if (model is EmployeeListModel.Loading) {
            if (model.list.isEmpty()) showBigProgress()
        } else hideBigProgress()
        if (model is EmployeeListModel.Error) {
            if (model.list.any()) showError(model)
            else showBigError(model.error)
        }
        else hideError()
    }

    private fun hideBigProgress() {
        progress.visibility = View.GONE
    }

    private fun showBigProgress() {
        progress.visibility = View.VISIBLE
    }

    private fun showBigError(message: String) {
        errorText.visibility = View.VISIBLE
        tryAgainButton.visibility = View.VISIBLE
        errorText.text = message
    }

    private var snackbar: Snackbar? = null

    private fun showError(model: EmployeeListModel.Error) {
        val snackbar = Snackbar.make(
            viewFinder.findViewById(android.R.id.content),
            model.error,
            Snackbar.LENGTH_LONG
        ).addCallback(snackbarCallback)
        this.snackbar = snackbar
        snackbar.show()
    }

    private fun hideError() {
        errorText.visibility = View.GONE
        tryAgainButton.visibility = View.GONE
        snackbar?.dismiss()
        snackbar = null
    }

    private val snackbarCallback = object : Snackbar.Callback() {
        override fun onDismissed(transientBottomBar: Snackbar?, event: Int) {
            super.onDismissed(transientBottomBar, event)
            onErrorDismiss()
        }
    }

}

class EmployeeViewHolder(layoutInflater: LayoutInflater, root: ViewGroup, private val onClick: (id: EmployeeId) -> Unit) :
    RecyclerView.ViewHolder(layoutInflater.inflate(R.layout.view_employee_summary, root, false)) {

    init {
        itemView.setOnClickListener {
            val id = id
            if (id != null)
                onClick(id)
        }
    }

    private var id: EmployeeId? = null

    private val image = itemView.findViewById<ImageView>(R.id.image)
    private val textName = itemView.findViewById<TextView>(R.id.textName)
    private val textTeam = itemView.findViewById<TextView>(R.id.textTeam)

    fun bind(employee: EmployeeSummary) {
        id = employee.id
        textName.text = employee.name
        textTeam.text = employee.team
        val photoId = employee.photoId
        if (photoId == null) {
            image.setImageResource(R.drawable.ic_profile_default_24dp)
        } else {
            val uri = Uri.parse(photoId.thumbnailUri)
            Picasso.get().load(uri).fit().centerCrop().placeholder(R.drawable.ic_profile_default_24dp).into(image)
        }
    }
}

class EmployeeListAdapter(private val layoutInflater: LayoutInflater, private val onClick: (id: EmployeeId) -> Unit) : RecyclerView.Adapter<EmployeeViewHolder>() {

    var employees: List<EmployeeSummary> by Delegates.observable(emptyList()) { _, _, _ ->
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = EmployeeViewHolder(layoutInflater, parent, onClick)

    override fun getItemCount() = employees.size

    override fun onBindViewHolder(holder: EmployeeViewHolder, position: Int) = holder.bind(employees[position])
}
