package materialize.square.directory.app.factory

import materialize.square.directory.app.repository.MemoryCachedRetrofitEmployeeRepository

val employees = MemoryCachedRetrofitEmployeeRepository()