package materialize.square.directory.app.factory

import materialize.square.directory.*
import materialize.square.directory.mobile.EmployeeListIntent
import materialize.square.directory.mobile.EmployeeListModel
import materialize.square.directory.mobile.EmployeeListPresenter

class EmployeeListControllerFactory {

    private val interactor = ViewEmployeeListInteractor(employees)

    fun create() = LateBindingEmployeeListController(interactor)
}

class LateBindingEmployeeListController(
    private val viewEmployeeList: ViewEmployeeList
) {

    private lateinit var delegate: suspend (EmployeeListIntent) -> Unit

    fun setRenderer(render: (EmployeeListModel) -> Unit) {
        delegate = EmployeeListPresenter(viewEmployeeList, render)::invoke
    }

    suspend operator fun invoke(intent: EmployeeListIntent) = delegate(intent)
}
