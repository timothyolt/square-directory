package materialize.square.directory.app.factory

import android.telephony.PhoneNumberUtils
import materialize.square.directory.ViewEmployee
import materialize.square.directory.ViewEmployeeInteractor
import materialize.square.directory.mobile.EmployeeIntent
import materialize.square.directory.mobile.EmployeeModel
import materialize.square.directory.mobile.EmployeePresenter

class EmployeeControllerFactory {

    private val interactor = ViewEmployeeInteractor(employees)

    private fun phoneFormatter(unformatted: String) = PhoneNumberUtils.formatNumber(unformatted, "US")

    fun create() = LateBindingEmployeeController(interactor, ::phoneFormatter)
}

class LateBindingEmployeeController(
    private val viewEmployee: ViewEmployee,
    private val phoneFormatter: (String) -> String
) {

    private lateinit var delegate: suspend (EmployeeIntent) -> Unit

    fun setRenderer(render: (EmployeeModel) -> Unit) {
        delegate = EmployeePresenter(viewEmployee, phoneFormatter, render)::invoke
    }

    suspend operator fun invoke(intent: EmployeeIntent) = delegate(intent)
}