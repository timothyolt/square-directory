package materialize.square.directory.test.doubles

import android.view.View
import materialize.square.directory.app.utils.ViewFinder

class ViewFinderStub(private val root: View) : ViewFinder {

    @Suppress("UNCHECKED_CAST")
    override fun <T : View> findViewById(id: Int) = root.findViewById<T>(id)

}