package materialize.square.directory.test

import android.view.LayoutInflater
import android.widget.ListView
import android.widget.TextView
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import materialize.square.directory.EmployeeId
import materialize.square.directory.EmployeeSummary
import materialize.square.directory.app.R
import materialize.square.directory.app.EmployeeListAdapter
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotNull
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class EmployeeListAdapterTest {

    @Test fun testItemCount() {
        val context = InstrumentationRegistry.getInstrumentation().targetContext
        val layoutInflater = context.getSystemService(LayoutInflater::class.java)

        assertNotNull(layoutInflater)

        val adapter = EmployeeListAdapter(layoutInflater!!, {})
        adapter.employees = listOf(
            EmployeeSummary(EmployeeId("Tim"), "Tim", "A-Team", null),
            EmployeeSummary(EmployeeId("Tom"), "Tom", "A-Team", null)
        )

        assertEquals(2, adapter.itemCount)
    }

    @Test fun testBinding() {
        val context = InstrumentationRegistry.getInstrumentation().targetContext
        val layoutInflater = context.getSystemService(LayoutInflater::class.java)

        assertNotNull(layoutInflater)

        val adapter = EmployeeListAdapter(layoutInflater!!, {})
        adapter.employees = listOf(
            EmployeeSummary(EmployeeId("Tim"), "Tim", "A-Team", null),
            EmployeeSummary(EmployeeId("Tom"), "Tom", "A-Team", null)
        )
        val position = 0

        val viewGroup = ListView(context)
        val viewHolder = adapter.onCreateViewHolder(viewGroup, adapter.getItemViewType(position))
        adapter.onBindViewHolder(viewHolder, position)

        val text = viewHolder.itemView.findViewById<TextView>(R.id.text).text
        assertEquals(adapter.employees[position].name, text)
    }
}
