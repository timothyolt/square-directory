package materialize.square.directory.test

import android.view.LayoutInflater
import android.view.View
import android.widget.ProgressBar
import android.widget.TextView
import androidx.test.platform.app.InstrumentationRegistry
import materialize.square.directory.EmployeeId
import materialize.square.directory.app.EmployeeView
import materialize.square.directory.app.R
import materialize.square.directory.mobile.EmployeeModel
import materialize.square.directory.test.doubles.ViewFinderStub
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotNull
import org.junit.Test

class EmployeeViewTest {
    @Test fun testRenderLoading() {
        val context = InstrumentationRegistry.getInstrumentation().targetContext
        val layoutInflater = context.getSystemService(LayoutInflater::class.java)

        assertNotNull(layoutInflater)

        val layout = layoutInflater!!.inflate(R.layout.activity_employee_detail, null)
        val viewFinder = ViewFinderStub(layout)

        val viewController = EmployeeView(viewFinder, {}, {})
        viewController.render(EmployeeModel.Loading)

        val progressBar = layout.findViewById<ProgressBar>(R.id.progress)
        assertEquals(progressBar.visibility, View.VISIBLE)

        val text = layout.findViewById<TextView>(R.id.textName)
        assertEquals(text.visibility, View.GONE)
    }

    @Test fun testRenderDone() {
        val context = InstrumentationRegistry.getInstrumentation().targetContext
        val layoutInflater = context.getSystemService(LayoutInflater::class.java)

        assertNotNull(layoutInflater)

        val layout = layoutInflater!!.inflate(R.layout.activity_employee_detail, null)
        val viewFinder = ViewFinderStub(layout)

        val viewController = EmployeeView(viewFinder, {}, {})
        val name = "Joe"
        viewController.render(EmployeeModel.Done(EmployeeId(name), name, "team", null, null, "joe@dirt.gov", null, "No Time"))

        val progressBar = layout.findViewById<ProgressBar>(R.id.progress)
        assertEquals(progressBar.visibility, View.GONE)

        val text = layout.findViewById<TextView>(R.id.textName)
        assertEquals(text.visibility, View.VISIBLE)
    }
}