package materialize.square.directory.test

import android.view.LayoutInflater
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import materialize.square.directory.EmployeeId
import materialize.square.directory.EmployeeSummary
import materialize.square.directory.app.EmployeeListAdapter
import materialize.square.directory.app.EmployeeListView
import materialize.square.directory.app.R
import materialize.square.directory.mobile.EmployeeListModel
import materialize.square.directory.test.doubles.ViewFinderStub
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotNull
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class EmployeeListViewTest {

    @Test fun testRenderEmployeeList() {
        val context = InstrumentationRegistry.getInstrumentation().targetContext
        val layoutInflater = context.getSystemService(LayoutInflater::class.java)

        assertNotNull(layoutInflater)

        val layout = layoutInflater!!.inflate(R.layout.activity_employee_list, null)
        val viewFinder = ViewFinderStub(layout)

        val viewController = EmployeeListView(viewFinder, {}, {}, {}, layoutInflater)

        val employeeList = EmployeeListModel.Done(listOf(
            EmployeeSummary(EmployeeId("Tim"), "Tim", "A-Team", null),
            EmployeeSummary(EmployeeId("Tom"), "Tom", "A-Team", null)
        ))
        viewController.render(employeeList)

        val adapter = layout.findViewById<RecyclerView>(R.id.list).adapter as EmployeeListAdapter
        assertEquals(employeeList.list, adapter.employees)
    }

    @Test fun testRenderLoadingWithList() {
        val context = InstrumentationRegistry.getInstrumentation().targetContext
        val layoutInflater = context.getSystemService(LayoutInflater::class.java)

        assertNotNull(layoutInflater)

        val layout = layoutInflater!!.inflate(R.layout.activity_employee_list, null)
        val viewFinder = ViewFinderStub(layout)

        val viewController = EmployeeListView(viewFinder, {}, {}, {}, layoutInflater)

        val employeeListLoading = EmployeeListModel.Loading(listOf(
            EmployeeSummary(EmployeeId("Tim"), "Tim", "A-Team", null),
            EmployeeSummary(EmployeeId("Tom"), "Tom", "A-Team", null)
        ))
        viewController.render(employeeListLoading)

        val adapter = layout.findViewById<RecyclerView>(R.id.list).adapter as EmployeeListAdapter
        assertEquals(employeeListLoading.list, adapter.employees)

        val swipeRefresh = layout.findViewById<SwipeRefreshLayout>(R.id.swipeRefresh)
        assertEquals(true, swipeRefresh.isRefreshing)
    }
}
